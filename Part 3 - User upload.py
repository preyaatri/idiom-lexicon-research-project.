import csv
import docx
from docx import Document
import pandas as pd
import collections
import enchant
import docx2txt
import string

##Read the idioms dictionary that has been created by the python script of part 1.
d = pd.read_csv("Idioms.csv")
bolds = d["Idioms"].tolist()

file_names = []
words = []
user_file = str(input("Enter name of the article/book in Docx format : "))

if ".docx" not in user_file:
    print("Invalid file")
    exit(0)

##Reading book/article that the user has uploaded to be analyzed.
file = docx.Document(user_file)
l = []
##Picking up paragraphs in the books.
for para1 in file.paragraphs:
    for run in para1.runs:
        ##Iterating the list of idioms that are contained in Idioms.csv file
        for idiom in bolds:
            ##Checking if any of the idiom in that list is conatined in text of all the paragraphs.
            if idiom in run.text:
                ##appending the idioms found to a list.
                l.append(idiom)

##Calculating the number of idioms present in a book
number_of_idioms = len(l)

##Frequency count of each idiom in the book
counter = collections.Counter(l)
freq_dict = dict(counter)
filename = user_file[:-5] +'.csv'
print("File containing the results :-",filename)

##Creating a csv file with the same name as that of the book, which will contain all the results obtained until now.
with open(filename, 'w', newline='') as f:
    writer = csv.writer(f, delimiter=',')
    ##header for the csv file.
    header = ['Book name','Idiom','Frequency']
    writer.writerow(header)
    ##transferring the results in the form of rows to the csv file.
    for key in freq_dict.keys():
        l = [filename[:-5] + ".docx", key, freq_dict[key]]
        writer.writerow(l)
        
##########################Creating Files with word and Idiom count#############################       

##For comparing the words with the words captured in the US dictionary.
d = enchant.Dict("en_US")

##Convert format of the book that has to be analyzed from Docx to TXT.
MY_TEXT = docx2txt.process(user_file)

##Creating txt format of the same book.
with open(user_file[:-5] +'.txt', "w") as textfile:
    print(MY_TEXT, file=textfile)

##Acessing the TXT format to count the number of words.
with open(user_file[:-5] +'.txt' , 'r', encoding='utf-8') as fi:
    for line in fi:
        for word in line.split(" "):
            if(word and d.check(word)):
               words.append(word)

##Calculating the number of words in the book.
number_of_words = len(words)

######################################Printing the results to a CSV file.##################################
header = ['Book name', 'Number of Idioms', 'Number of words']

##File containing the final results.
with open("Idiom_count.csv", "w", newline='') as f:
    writer = csv.writer(f, delimiter=',')
    writer.writerow(header) # write the header
    row = [filename[:-5] + ".docx", number_of_idioms, number_of_words]
    writer.writerow(row)
